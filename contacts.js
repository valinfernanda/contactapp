//https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_file_system

const fs = require("fs");
const chalk = require("chalk");
const validator = require("validator");

//membuat folder data baru : mkdirSync
const dirPath = "./data";
if (!fs.existsSync(dirPath)) {
  fs.mkdirSync(dirPath);
}

//membuat file contacts.json
//membuat file baru : writeFileSync (lokasi, isi default, encodingnya )
const dataPath = "./data/contacts.json";
if (!fs.existsSync(dataPath)) {
  fs.writeFileSync(dataPath, "[]", "utf-8");
}

const loadContact = () => {
  const file = fs.readFileSync("./data/contacts.json", "utf-8");
  const contacts = JSON.parse(file);
  return contacts;
};

const simpanContact = (nama, email, noHP) => {
  const contact = {
    nama: nama,
    email: email,
    noHP: noHP,
  };
  const contacts = loadContact();

  //cek duplikat
  const duplikat = contacts.find((contact) => contact.nama === nama);
  if (duplikat) {
    console.log(
      chalk.red.inverse.bold("Contact udah terdaftar, gunakan nama lain")
    );
    return false;
  }

  //cek email
  if (email) {
    if (!validator.isEmail(email)) {
      console.log("Email tidak valid");
      return false;
    }
  }

  //cek no HP
  if (!validator.isMobilePhone(noHP, "id-ID")) {
    console.log("Nomor HP tidak valid");
    return false;
  }

  contacts.push(contact);

  fs.writeFileSync("./data/contacts.json", JSON.stringify(contacts));

  console.log(`Terimakasih sudah memasukkan data`);
};

const listContact = () => {
  const contacts = loadContact();
  console.log("Daftar Kontak");
  contacts.forEach((contact, i) => {
    console.log(`${i + 1}. ${contact.nama} - ${contact.noHP}`);
  });
};

const detailContact = (nama) => {
  const contacts = loadContact();
  const contact = contacts.find(
    (contact) => contact.nama.toLowerCase() === nama.toLowerCase()
  );

  if (!contact) {
    console.log(`Nama ${nama} tidak ditemukan`);
    return false;
  }

  console.log(contact.nama);
  console.log(contact.noHP);

  if (contact.email) {
    console.log(contact.email);
  }
};

const deleteContact = (nama) => {
  const contacts = loadContact();
  const newContacts = contacts.filter(
    (contact) => contact.nama.toLowerCase() !== nama.toLowerCase()
  );

  if (contacts.length === newContacts.length) {
    console.log(`${nama} tidak ditemukan, jadi tidak bisa dihapus`);
    return false;
  }
  fs.writeFileSync("./data/contacts.json", JSON.stringify(newContacts));
  console.log(`data contact ${nama} berhasil dihapus`)

};

//kalo find, sekali ketemu prosesnya langsung berenti
//kalo filter, dia bakal nyari terus sampe urutan di arraynya selesai

module.exports = { simpanContact, listContact, detailContact, deleteContact };
