// // const contacts = require('./contacts')
const { command } = require("yargs");
const yargs = require("yargs");
const contacts = require("./contacts");

//command (add/etc, pesan, builder(function/obejct), handler )
yargs
  .command({
    command: "add",
    describe: "Menambah contact baru",
    builder: {
      nama: {
        describe: "Nama lengkap",
        demandOption: true,
        type: "string",
      },
      email: {
        describe: "Email",
        demandOption: false,
        type: "string",
      },
      noHP: {
        describe: "Nomor HP",
        demandOption: true,
        type: "string",
      },
    },
    handler(argv) {
      contacts.simpanContact(argv.nama, argv.email, argv.noHP);
    },
  })
  .demandCommand();
//demandCommand supaya kalo run node app, ada keterangan

//menampilkan daftar semua nama dan no.HP
yargs.command({
  command: "list",
  describe: "Menampilkan semua nama dan no.HP",
  handler() {
    contacts.listContact();
  },
});

//menampilkan detail sebuah kontak
yargs.command({
  command: "detail",
  describe: "Menampilkan detail sebuat contact berdasarkan nama",
  builder: {
    nama: {
      describe: "Nama lengkap",
      demandOption: true,
      type: "string",
    },
  },
  handler(argv) {
    contacts.detailContact(argv.nama);
  },
});

//menghapus contact berdasarkan nama
yargs.command({
  command: "delete",
  describe: "Menghapus sebuah contact berdasarkan nama",
  builder: {
    nama: {
      describe: "Nama lengkap",
      demandOption: true,
      type: "string",
    },
  },
  handler(argv) {
    contacts.deleteContact(argv.nama);
  },
});

yargs.parse();
//untuk menjalannkan yargs
